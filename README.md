# StreamUI

Basic template to create a Streamlit app for visualization, analysis and deployment via Dimension Dashboard.

## Installation

Use Dimensionops to create a project using the StreamUI template.

```bash
dimensionops create-project your_project_name your_git_username . streamui
```

## Usage
```bash
cd your_project_name/src
streamlit run app.py
```


## Project Organisation

    ├── data                    <- Data
    │
    ├── notebooks               <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                              the creator's initials, and a short `_` delimited description, e.g.
    │                              `1.0_KK_Initial_data_exploration`
    │
    ├── src                     <- Source code for use in this project
    │   │
    │   ├── __init__.py         <- Makes src a Python module
    │   │
    │   ├── conf                <- Configuration
    │   │   └── conf.txt        <- MongoDB configuration
    │   │
    │   │── setup               <- Basic setup files
    │   │   │── database.py     <- Connection to MongoDB
    │   │   └── layout.py       <- Basic layout of Streamlit app
    │   │ 
    │   └── visualization       <- Visualizations
    │       └── plot.py         <- Simple plot
    │ 
    │── Dockerfile              <- Dockerfile to assemble a Docker image
    │
    │── env.yml                 <- Environment yml file to create conda environment
    │
    ├── Makefile                <- Makefile with commands like `make data` or `make train`
    │
    ├── README.md               <- The top-level README for developers using this project
    │
    └── requirements.txt        <- The requirements file for reproducing the analysis environment